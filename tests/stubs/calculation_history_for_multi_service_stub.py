from .service_stub import ServiceStub
from .history_stub import HistoryStub


class CalculationHistoryForMultiServiceStub:
    history_map = {}

    def __init__(self, uncalculated_fees, second_uncalculated_fees):
        self.history_map['ServiceStub'] = HistoryStub(uncalculated_fees)
        self.history_map['SecondServiceStub'] = HistoryStub(second_uncalculated_fees)

    def retrieve_history(self, service):
        if type(service) is ServiceStub:
            return self.history_map.get('ServiceStub')
        return self.history_map.get('SecondServiceStub')

    def verify_applied_sum_for_service(self, expected_sum, service):
        if type(service) is ServiceStub:
            return self.history_map.get('ServiceStub').verify_applied_sum(expected_sum)
        return self.history_map.get('SecondServiceStub').verify_applied_sum(expected_sum)
